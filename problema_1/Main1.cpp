#include <iostream>

#include "Lista.h"

using namespace std;

int main(void){
    Lista *lista = new Lista();
    int can,opc;
    int i=0;

     do{
        cout << "\n";
        cout << "\t     ---------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
		cout << "\t     ---------------------------- " << endl;
		cout << "\n  [1]  > Ingresar un numero\t" << endl;
		cout << "  [2]  > Salir del programa\t" << endl;
      
        cout << "\nOpción: ";
        cin >> opc;
		cin.ignore();

        if (opc == 1){
            cout << "\n";
            cout << "Ingrese el numero: ";
			cin >> can;
			cout << endl;
			lista->agregar(can);
			lista->mostrar();
		}
        
        else if(opc == 2){
            delete lista;
            cout << "\n\t¡Se ha cerrado el programa correctamente!\n" << endl;
			break;  
        }

        else{
            cout << "\t--------------------------------------" << endl;
			cout << "\tOpción Invalida. " << endl;
			cout << "\t--------------------------------------\n" << endl;
        }
    }while(i<1);

    return 0;
}