#ifndef LISTA_H
#define LISTA_H
#include <iostream>

using namespace std;


//Estructura de nodo
typedef struct Nodo {
    int numero;
    struct Nodo *sig;
} Nodo;

class Lista{

    private:
        Nodo *first_n = NULL;

    public: 
        Lista();
        void agregar(int num);
        void mostrar();
};
#endif