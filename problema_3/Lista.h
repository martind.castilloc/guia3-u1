#ifndef LISTA_H
#define LISTA_H

#include <iostream>

using namespace std;

//ESTRUCTURA DEL NODO
typedef struct _Nodo {
    int numero;
    struct _Nodo *sig;
} Nodo;

class  Lista{

    private:
        Nodo *first_n = NULL;

    public:
        Lista();

        // Funcion que crea lista
        void agregar(int num);
        // Funcion que imprime la lista
        void mostrar();
        //Rellena la lista actual.
        void rellenar_lista();
};
#endif
