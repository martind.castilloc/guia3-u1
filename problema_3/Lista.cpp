#include <iostream>

#include "Lista.h"

using namespace std;

Lista::Lista(){}

void Lista::agregar(int num){

    //Se crea un nodo.
    Nodo *nod = new Nodo();
    nod->numero = num;
    nod->sig = NULL;

    if(this->first_n == NULL){
        this->first_n = nod;
    }else{
        Nodo *second, *nuevo;
        second = this->first_n;

        while (second !=NULL && second->numero < num){
            nuevo = second;
            second = second->sig;
        }

        if (second == this->first_n){
            this->first_n = nod;
            this->first_n->sig = second;
        } else{
            nuevo->sig = nod;
            nod->sig = second;
        }  
    }
}

// Funcion que imprime el contenido de las listas, usando un nodo
void Lista::mostrar(){
    //Utiliza variable temporal para recorrer la lista.
    Nodo *nod = this->first_n;

    while (nod != NULL){
        cout<< nod->numero <<endl;
        nod = nod->sig;
    }
    cout<<endl;
}


//Funcion que nos permirte rellenar la lista
void Lista::rellenar_lista(){
    Nodo *r,*s;
    r = this->first_n;

    while (r->sig != NULL){
        s = r;
        r = r->sig;

        if(r->numero - 1 > s->numero){
            for(int i=s->numero+1;i<r->numero;i++){
                agregar(i);
            }
        }
    }   
}