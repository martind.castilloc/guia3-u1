#ifndef LISTA_H
#define LISTA_H

#include <iostream>

using namespace std;

//ESTRUCTURA DEL NODO
typedef struct _Nodo {
    int numero;
    struct _Nodo *sig;
} Nodo;

class  Lista{

    private:
        Nodo *first_n = NULL;

    public:
        Lista();

        void agregar(int num);
        void mostrar();
        void unir_Lista(Lista *lista);
};
#endif


