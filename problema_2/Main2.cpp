#include <iostream>

#include "Lista.h"

using namespace std;

void crea_lista(Lista *lista){
    int can,opc;
    int i=0;

    do{
        cout << "\n";
        cout << "\t     ---------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
		cout << "\t     ---------------------------- " << endl;
		cout << "\n  [1]  > Ingresar un numero\t" << endl;
		cout << "  [2]  > Salir de la lista, para pasar a la otra.\t" << endl;
      
        cout << "\nOpción: ";
        cin >> opc;
		cin.ignore();

        if (opc == 1){
            cout << "\n";
            cout << "Ingrese el numero: ";
			cin >> can;
			cout << endl;
			lista->agregar(can);
			lista->mostrar();
		}
        
        else if(opc == 2){
            i=1; 
        }

        else{
            cout << "\t--------------------------------------" << endl;
			cout << "\tOpción Invalida. " << endl;
			cout << "\t--------------------------------------\n" << endl;
        }
    }while(i<1);
}

int main(void){
    // Se crean los objetos Lista.
    Lista *lista1 = new Lista();
    Lista *lista2 = new Lista();
    Lista *lista3 = new Lista();

    crea_lista(lista1);
	crea_lista(lista2);

    //Se crea una tercera lista uniendo la lista1 y lista2.
    lista3 -> unir_Lista(lista1);
    lista3 -> unir_Lista(lista2);

    //Se muetra la lista3.
    cout<<"\nLista 3: "<<endl;
    lista3 -> mostrar();

    // Se libera memoria del sistema.
    delete lista1;
    delete lista2;
    delete lista3;

    return 0;
}