#include <iostream>

#include "Lista.h"

using namespace std;

Lista::Lista(){}

//Funcion que agregra los numeros ingresados.
void Lista::agregar(int num){
    Nodo *nod = new Nodo();
    nod->numero = num;
    nod -> sig = NULL;

    if(this -> first_n == NULL){
        this -> first_n = nod;
    }else{
        Nodo *second, *nuevo;
        second = this->first_n;

        while (second !=NULL && second->numero < num){
            nuevo = second;
            second = second->sig;
        }

        if (second == this->first_n){
            this->first_n = nod;
            this->first_n->sig = second;
        } else{
            nuevo->sig = nod;
            nod->sig = second;
        }  
    }
}

//Funcion que imprime el contenido de las listas, usando un nodo.
void Lista::mostrar(){
    //Utiliza variable temporal para recorrer la lista.
    Nodo *nod = this->first_n;

    while (nod != NULL){
        cout<< nod->numero << endl;
        nod = nod->sig;
    }
    cout<<endl;
}

//Funcion que une las listas.
void Lista::unir_Lista(Lista *lista){
    Nodo *nod= lista -> first_n;
    while(nod != NULL){
        agregar(nod -> numero);
        nod = nod -> sig;
    }
}


